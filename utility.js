
import chalk from "chalk";
import gutil from "gulp-util";

/**
 * Whenever any plugin throws an error, this function will handle it.
 * @param  {Object} err
 * @return {Void}
 */
module.exports.errorHandler = (err) => {
  gutil.log(chalk.red(`Whoops, something has gone horribly wrong!\n\n${chalk.yellow(err)}\n\n`));
};

/**
 * Get Time Stamp
 * @return {String}
 */
module.exports.getTimeStamp = () => {
  let d = new Date();
  return ("0" + d.getHours()).slice(-2) + ":" + ("0" + d.getMinutes()).slice(-2) + ":" + ("0" + d.getSeconds()).slice(-2);
};
